import plotly
import plotly.plotly as py
import plotly.graph_objs as go
import plotly.figure_factory as FF

import numpy as np
import pandas as pd
plotly.tools.set_credentials_file(username='prachatosm', api_key='f7DunAEaw7u4VG42YFAY')
df = pd.read_csv("F:\\bci\\Experiment37WCW.csv")

sample_data_table = FF.create_table(df.head())
py.plot(sample_data_table, filename='sample-data-table')
trace1 = go.Scatter(x= df['timestamps'], y=df['TP10'], mode='lines', name='TP10' )
trace2 = go.Scatter(x=df['timestamps'], y=df['TP9'], mode='lines', name='TP9' )
trace3 = go.Scatter(x=df['timestamps'], y=df['Marker'], mode='lines', name='Marker' )
layout = go.Layout(title='Experiment I, Trial 2, Subject 2, 7 numbers displayed', plot_bgcolor='rgb(230, 230,230)', xaxis=dict(title='Timestamp (ms)'), yaxis=dict(title='EEG value (μV)'))
fig = go.Figure(data=[trace1, trace2, trace3], layout=layout)

# Plot data in the notebook
py.plot(fig, filename='Sample experiment for report')