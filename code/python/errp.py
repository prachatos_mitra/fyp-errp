import os
import sys
from collections import OrderedDict
from glob import glob
import mne
import pandas as pd
import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt
from mne import Epochs, find_events
from sklearn.cross_validation import cross_val_score, ShuffleSplit
from sklearn import svm
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LogisticRegression, ElasticNet
from sklearn.preprocessing import StandardScaler
from mne.preprocessing import Xdawn
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from mne.decoding import Vectorizer
from pyriemann.estimation import ERPCovariances
from pyriemann.tangentspace import TangentSpace
from pyriemann.classification import MDM
from mne.time_frequency import psd_multitaper
from mne.preprocessing import ICA
from sklearn.pipeline import make_pipeline
sys.path.append(os.getcwd()+"\\muse")
import utils
import musetils

class ErrpTrain:

	def __init__(self):
		self.subject = 6
		self.session = 1
	
	def run(self):
		#load data
		raw = musetils.load_data('visual\\ERRP', sfreq=256., subject_nb=self.subject, session_nb=self.session, choice=True)
		self.preprocess(raw)
	
	def preprocess(self, raw, train=True):
		#raw.plot()
		raw.plot_psd(tmax=np.inf); #use matplotlib to plot raw
		#Bandpass from 1 to 100
		raw.filter(l_freq=1,h_freq=100, method='iir', iir_params={'ftype': 'cheby2',  'order': 5, 'rs': 20})
		picks_eeg = mne.pick_types(raw.info ,eeg=True, meg=False, eog=False, stim=False, exclude='bads')
		method = 'fastica'

		# Choose other parameters
		n_components = 4  # if float, select n_components by explained variance of PCA
		decim = 3  # we need sufficient statistics, not all time points -> saves time

		# we will also set state of the random number generator - ICA is a
		# non-deterministic algorithm, but we want to have the same decomposition
		# and the same order of components each time this tutorial is run
		random_state = 23

		ica = ICA(n_components=n_components, method=method, random_state=random_state)
		print(ica)
		reject = dict(mag=200e-6)
		ica.fit(raw, picks=picks_eeg, decim=decim, reject=reject)
		print(ica)
		#ica.plot_components() 
		#ica.plot_properties(raw, picks=[0,1, 2, 3], psd_args={'fmax': 35.})

		#raw.append(raw1)
		if not train:
			return self.get_events(raw, ica, train)
		self.get_events(raw, ica, train)
	
	def get_events(self, raw, ica, train=True):
		events = find_events(raw) #find events, refer MNE API for functionality
		event_id = {'p300': 1, 'errp':2 } #label
		#ok now we can use labeled events to find epochs
		if not train:
			return self.epochs(raw, ica, events,  event_id, train)
		return self.epochs(raw, ica, events, event_id, train)
	
	def epochs(self, raw, ica, events, event_id, train=True):
		#Load an MNE Epoch object
		#parameters -> raw data, events, event_id
		#Reject values < 75e-6 (from literature)
		#Picks lists selected electrodes
		#Channel AF7 has low correlation with ErrP (or other epochs for that matter)
		#Therefore, drop AF7 and select other 3 channels
		epochs = Epochs(raw,events=events,event_id=event_id, tmin=0,tmax=0.8, baseline=None,reject={'eeg': 400e-6}, preload=True,verbose=False,picks=[0,3]) #why 75e-6
		print(epochs)
		print(epochs.drop_log)
		conditions = OrderedDict()
		conditions['p300'] = [1]
		conditions['errp'] = [2]
		#Plot log of dropped epochs
		epochs.plot_drop_log()
		#Now average out targets and non targets and plot their EEG
		evoked_target = epochs['p300'].average()
		evoked_nontarget = epochs['errp'].average()
		fig = evoked_target.plot()
		fig = evoked_nontarget.plot()
		epochs.plot(title='Epochs', block=True)
		#Plot the 3 channels with 2 waveforms (target and nontarget epochs)
		#fig, ax = musetils.plot_conditions(epochs, conditions=conditions,ci=97.5,ylim=(-20,20),n_boot=2000,title='',diff_waveform=(2,1))
		print(epochs.standard_error())
		epochs.plot_psd(fmin=1, fmax=110)
		epochs.plot_sensors(block=True)
		epochs.plot(block=True)
		if not train:
			return self.classify(epochs, events, event_id, train)
		#ok now classify
		return self.classify(epochs, events, event_id, train)
		
	def classify(self, epochs, events, event_id, train=True):
		epochs.pick_types(eeg=True)
		if train:
			xdf = Xdawn(2)
			n_epochs_40 = len(epochs) * 0.4
			start, stop = int(len(epochs) - n_epochs_40), epochs.__len__() - 1
			xdf.fit(epochs[start:stop])
			epochs_den = xdf.apply(epochs)
		else:
			epochs_den = epochs
		epochs_errp = epochs_den['errp']
		X = epochs_errp.get_data() * 1e6
		times = epochs.times
		print(times)
		y = epochs_errp.events[:, -1]
		psd, freq = psd_multitaper(epochs_errp, fmin=30,fmax=100)
		print(psd.shape)
		print(y)
		ec = 0
		cc = 0
		tc = 0
		for ec in range(0, X.shape[0]):
			for cc in range(0, X.shape[1]):
				for tc in range(0, X.shape[2]):
					if tc % 4 == 0 and tc < X.shape[2] - 3:  
						X[ec, cc, tc] = (X[ec, cc, tc] + X[ec, cc, (tc + 1)] + X[ec, cc, (tc + 2)] + X[ec, cc, (tc + 3)])/4.0
						X[ec, cc, (tc + 3)] = X[ec, cc, (tc + 2)] = X[ec, cc, (tc + 1)] = X[ec, cc, tc]
		for ec in range(0, psd.shape[0]):
			for cc in range(0, psd.shape[1]):
				for tc in range(0, X.shape[2]):
					if tc % 5 == 0 and tc < psd.shape[2] - 4:  
						psd[ec, cc, tc] = (psd[ec, cc, tc] + psd[ec, cc, (tc + 1)] + psd[ec, cc, (tc + 2)] + psd[ec, cc, (tc + 3)] + psd[ec, cc, (tc + 4)])/4.0
						psd[ec, cc, (tc + 4)] = psd[ec, cc, (tc + 3)] = psd[ec, cc, (tc + 2)] = psd[ec, cc, (tc + 1)] = psd[ec, cc, tc]				
		for ec in range(0, X.shape[0]):
			for cc in range(0, X.shape[1]):
				index = []
				st_val = set(X[ec, cc])
				lst_val = list(st_val)
				for tc in range(0, len(lst_val)):
					X[ec, cc, tc] = lst_val[tc]
				st_psd = set(psd[ec, cc])
				lst_psd = list(st_psd)
				for tc in range(len(lst_val), len(lst_val) + len(lst_psd)):
					X[ec, cc, tc] = lst_psd[tc - len(lst_val)]
		X_new = np.resize(X, (X.shape[0], X.shape[1], int(len(lst_val) + len(lst_psd))))
		if not train:
			return X_new
		#Use a linear SVM
		clf = [svm.SVC(C=1, kernel='linear'), LDA(shrinkage='auto', solver='eigen')]
		
		for i in range(0,2):
			if not i:
				cname = 'LDA'
			else: cname = 'SVM'
			clfpp  = make_pipeline(Vectorizer(),clf[i])
			cv = ShuffleSplit(len(X), 10, test_size=0.25, random_state=42)
			X_2d = X.reshape(len(X), -1)
			X_2d = X_2d / np.std(X_2d)
			print(X_new.shape)
			#Ok, now get score
			scores_full = cross_val_score(clfpp, X_new, y, cv=cv, scoring='f1', n_jobs=1)
			score_min = 99999999
			for score in scores_full:
				if score_min > score:
					score_min = score
			print("Classification score for " + cname + ": %s (std. %s)" % (np.mean(scores_full), np.std(scores_full)) + ", minimum " + str(score_min))
			clfpp.fit(X_new, y)
		#self.validate(clf)
		
	def validate(self, clf):
		raw = musetils.load_data('visual\\ERRP', sfreq=256., subject_nb=6, session_nb=self.session, choice=True)
		X = self.preprocess(raw, False)
		
		print(clf.predict(X))


ErrpTrain().run()