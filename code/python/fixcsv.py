import os, re

class PreProcess:
	def __init__(self, path):
		self.path = path
		self.add_headers()
		self.average_out_readings()
		
	def add_headers(self):
		for filename in os.listdir(self.path):
			file = self.path + '\\' + filename
			with open(file, 'r') as original: data = original.read()
			with open(file, 'w') as modified: modified.write("timestamps,TP9,AF7,AF8,TP10,Marker0\n" + data)
		return True

	def average_out_readings(self):
		for filename in os.listdir(self.path):
			file = self.path + '\\' + filename
			with open(file, 'r') as original: data = original.read()
			output = ""
			lines = data.split('\n')
			first = True
			for line in lines:
				content = line.split(',')
				if not first and len(content) > 4:
					output = output + content[0] + ','
					TP9 = float(content[1])
					AF7 = float(content[2])
					AF8 = float(content[3])
					TP10 = float(content[4])
					avg = (TP9 + AF7 + AF8 + TP10)/4.0
					TP9 = TP9 - avg
					AF7 = AF7 - avg
					AF8 = AF8 - avg
					TP10 = TP10 - avg
					output = output + str(TP9) + ',' + str(AF7) + ',' + str(AF8) + ',' + str(TP10) + ',' + content[5]
				else:
					first = False
					output = output + line
				output = output + '\n'
			with open(file, 'w') as modified: modified.write(output)
		return True

	def replace_marker(self):
		for filename in os.listdir(self.path):
			file = self.path + '\\' + filename
			if re.match(".*CCC.csv", file) or re.match(".*WCC.csv", file):
				with open(file, 'r') as original: data = original.read()
				output = ""
				lines = data.split('\n')
				first = True
				for line in lines:
					content = line.split(',')
					if not first and len(content) > 4:
						print(content)
						if content[5] == '2':
							content[5] = '0'
						output = output + content[0] + ',' + content[1] + ',' + content[2] + ',' + content[3] + ',' + content[4] + ',' + content[5]
					else:
						first = False
						output = output + line
					output = output + '\n'
				with open(file, 'w') as modified: modified.write(output)
		return True

p = PreProcess("F:\\bci\\Subject3")